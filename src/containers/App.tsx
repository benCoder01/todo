import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State, ActionTodo, AppProps, AppActionProps, Todo } from "../interfaces";

import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Grid from "material-ui/Grid";

import TodoList from "../components/TodoList";
import TodoInput from "../components/TodoInput";

import "./App.css";
// TODO: Custom interfaces for TodoList and TodoInput

class App extends React.Component<AppProps & AppActionProps> {
    render() {
        return (
            <div className="App">
                <AppBar
                    position="sticky"
                    color="default"
                    className="bar"
                    style={{ margin: 0}}
                >
                    <Toolbar>
                        <Typography variant="title" color="inherit">
                            YourTodo
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className="content">
                    <Grid container={true} spacing={16} justify={"center"}>
                        <Grid item={true} xs={12}>
                            <TodoInput
                                addTodo={this.props.addTodo}
                                removeTodo={this.props.removeTodo}
                                todos={this.props.todos}
                            />
                        </Grid>

                        <Grid item={true} xs={12} md={6}>
                            <TodoList
                                todos={this.props.todos}
                                removeTodo={this.props.removeTodo}
                            />
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }
}

/**
 * Which properties of the store should be used in this component
 * @param {State} state
 * @returns {AppProps}
 */
const mapStateToProps = (state: State): AppProps => {
  return {
      todos: state.todos,
  };
};

/**
 * Which actions of the store should be used in this component
 * @param {Dispatch<ActionTodo>} dispatch
 * @returns {AppActionProps}
 */
const mapDispatchToProps = (dispatch: Dispatch<ActionTodo>): AppActionProps => {
  return {
      addTodo: (todo: Todo) => {
          dispatch({
              type: "ADD",
              payload: todo,
          });
      },
      removeTodo: (todo: Todo) => {
          dispatch({
              type: "REMOVE",
              payload: todo,
          });
      }
  };
};

export default connect<AppProps, AppActionProps, {}>(mapStateToProps, mapDispatchToProps)(App);
