import { createStore, combineReducers } from "redux";
import todos from "./reducers/todoReducer";

export const store = createStore(combineReducers(
    {
        todos,
    })
);

// createStore(initialMessageState, messageReducer)