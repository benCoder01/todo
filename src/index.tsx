import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import { store } from "./store";
import { Provider } from "react-redux";
import Reboot from "material-ui/Reboot";

// Provider makes the store accessible to all components
ReactDOM.render(
    <div>
        <Reboot/>
        <Provider store={store}>
            <App/>
        </Provider>
    </div>,
    document.getElementById("root") as HTMLElement
);
registerServiceWorker();
