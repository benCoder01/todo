export interface State {
    todos: Todos;
}

export interface AppProps {
    todos: Todos;
}

export interface AppActionProps {
    addTodo: (todo: Todo) =>  void;
    removeTodo: (todo: Todo) => void;
}

export interface Todo {
    text: String;
    id: number;
}

export interface Todos {
    todos: Todo[];
}

export interface ActionTodo {
    type: String;
    payload: Todo;
}

export interface TodoListProps {
    todos: Todos;
    removeTodo: (todo: Todo) => void;
}