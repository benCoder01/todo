import { ActionTodo, Todos } from "../interfaces";

const initialTodoState: Todos = {
    todos: [
        {
            text: "Klarinette spielen",
            id: 0,
        },
        {
            text: "Klarinette spielen",
            id: 1,
        },
    ],
};

const todoReducer = (state = initialTodoState, action: ActionTodo) => {
    switch (action.type) {
        case "ADD":
            return {
                todos: [
                    ...state.todos,
                    action.payload,
                ]
            };

        case "REMOVE":
            const todos = [...state.todos];
            const index = todos.indexOf(action.payload);

            if (index > -1) {
                todos.splice(index, 1);
                console.log(index);
            }

            return {
                todos: todos,
            };

        default:
            break;
    }
    return state;
};

export default todoReducer;