import * as React from "react";
import { AppActionProps, AppProps, Todo } from "../interfaces";
import TextField from "material-ui/TextField";
import Button from "material-ui/Button";
import AddIcon from "material-ui-icons/Add";

import "./TodoInput.css";

class TodoInput extends React.Component<AppProps & AppActionProps>  {
    state = {
        text: "",
    };

    updateText = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({text: event.currentTarget.value});
    }

    createTodo = (): Todo => {
        const todos = [...this.props.todos.todos];
        let lastID = -1;
        if (todos.length > 0) {
            lastID = todos[todos.length - 1].id;
        }

        const todo = {
            text: this.state.text,
            id: lastID + 1,
        };

        this.setState({text: ""});

        return todo;
    }

    render() {
        return (
            <div className="wrapper">
                <TextField onChange={this.updateText} value={this.state.text} className="TextField"/>
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="add"
                    onClick={() => this.props.addTodo(this.createTodo())}
                    className="Button"
                >
                    <AddIcon />
                </Button>
            </ div>
        );
    }
}

export default TodoInput;