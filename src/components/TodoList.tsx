import * as React from "react";
import { TodoListProps, Todo } from "../interfaces";
import IconButton from "material-ui/IconButton";
import DeleteIcon from "material-ui-icons/Delete";
import List, { ListItem, ListItemSecondaryAction, ListItemText } from "material-ui/List";
import Divider from "material-ui/Divider";

import "./TodoList.css";

export const TodoList = (props: TodoListProps) => {
    return (
        <div className="wrapper">
            <List>
                {props.todos.todos.map((todo: Todo) => {
                    return (
                        <div key={todo.id}>
                            <Divider/>
                            <ListItem key={todo.id}>
                                <ListItemText primary={todo.text} />
                                <ListItemSecondaryAction>
                                    <IconButton onClick={() => props.removeTodo(todo)} aria-label="Delete">
                                        <DeleteIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </div>
                    );
                })}
            </List>
        </div>
    );
};

export default TodoList;